<?php

declare(strict_types=1);

require 'Model/Job.php';
require 'JobCalculatorInterface.php';

use Model\Job;

/**
 * Class JobCalculator
 */
class JobCalculator implements JobCalculatorInterface
{
    const REQUIRES = 'requires';
    const OR = 'or';
    const AND = 'and';

    private $myAssets = [];
    private $jobs = [];


    /**
     * @param array $myAssets
     */
    public function __construct(array $myAssets)
    {
        $this->myAssets = $myAssets;
    }

    /**
     * Transforming and formatting jobs description to an array of Job objects,
     * so the job filtering will be smoother
     *
     * @param array $jobBoardListing
     * @return Model\Job[]
     */
    public function findSuitableJobs(array $jobBoardListing): array
    {
        $formattedJobs = $this->stringJobsToArray($jobBoardListing);

        $evaluatedJobs = $this->iterateAssetsAndJobs($formattedJobs);

        $this->mergeJobEvaluationsByMyAssets($evaluatedJobs);

        return $this->jobs;
    }

    /**
     * Prepare array of a string jobs and transforms into an array
     *
     * @param array $jobBoardListing
     * @return array
     */
    private function stringJobsToArray(array $jobBoardListing): array
    {
        $formattedJobs = [];

        foreach ($jobBoardListing as $key => $jobRequirement) {
            $jobRequirements = $this->purifyJobString($jobRequirement);

            if (!($jobRequirements = $this->checkIfJobHasRequires($jobRequirements, $key))) {
                continue;
            }

            $formattedJobs[$key] = $this->jobWordsToArray($jobRequirements);
        }

        return $formattedJobs;
    }

    /**
     * Removes non-used parts of job requirement
     *
     * @param $jobRequirement
     * @return string
     */
    private function purifyJobString(string $jobRequirement): string
    {
        //remove a dot
        $jobRequirement = str_replace('.', '', $jobRequirement);

        //remove prepositions a, an
        $jobRequirement = str_replace([' a ', ' an '], ' ', $jobRequirement);

        $lastQuote = strrpos($jobRequirement, '"');

        //remove a company name
        $restOfTheRequirements = substr($jobRequirement, $lastQuote + 2);

        return $restOfTheRequirements;
    }

    /**
     * Creates Job object and adds it to jobs array
     *
     * @param string|null $myAsset
     * @param int $jobKey
     * @return void
     */
    private function addFittingJob($myAsset, int $jobKey): void
    {
        $job = new Job($myAsset, $jobKey);
        $this->jobs[] = $job;
    }

    /**
     * Check if job requirements is actually requiring any skill
     *
     * @param string $restOfTheRequirements
     * @param int $key
     * @return bool|string
     */
    private function checkIfJobHasRequires(string $restOfTheRequirements, int $key)
    {
        $result = false;

        $requiredIsFirstWord = substr($restOfTheRequirements, 0, 8) == "requires";

        if ($requiredIsFirstWord) {
            //remove word 'requires'
            $result = ltrim(strstr($restOfTheRequirements, " "));
        } else {
            //Job description says "doesn't require anything"
            $this->addFittingJob(null, $key);
        }

        return $result;
    }

    /**
     * Separates a sentence by comma as a logical operator and evaluates sentence words
     *
     * @param string $jobRequirements
     * @return array
     */
    private function jobWordsToArray(string $jobRequirements): array
    {
        $clauseParts = [];

        $jobRequirements = addslashes($jobRequirements);

        //find commas as a separator
        $jobRequirementsParts = explode(",", $jobRequirements);

        foreach ($jobRequirementsParts as $key => $sentencePart) {
            $clauseParts = array_merge($clauseParts, $this->evaluateSentencePartWords($sentencePart, $key));
        }

        return $clauseParts;
    }

    /**
     * Iterates over my assets and job requests
     *
     * @param array $formattedJobs
     * @return array
     */
    private function iterateAssetsAndJobs(array $formattedJobs): array
    {
        $evaluatedJobs = [];

        //iterate over my assets and formatted job list
        foreach ($this->myAssets as $myAsset) {
            $evaluatedJobs = array_merge($evaluatedJobs, $this->iterateJobsByAsset($formattedJobs, $myAsset));
        }

        return $this->mergeSentenceResults($evaluatedJobs);
    }

    /**
     *Merges job sentences results
     *
     * @param array $evaluatedJobs
     * @return array
     */
    private function mergeSentenceResults(array $evaluatedJobs): array
    {
        $mergedJobs = [];

        //merge sentence parts results by my assets
        foreach ($evaluatedJobs as $key => $evaluatedJobByMyAssets) {
            foreach ($evaluatedJobByMyAssets as $myAsset => $jobSegmentResults) {
                $res = false;
                foreach ($jobSegmentResults as $jobSegmentResult) {
                    $mergedJobs[$key][$myAsset] = $res || $jobSegmentResult;
                    $res = $jobSegmentResult;
                }
            }
        }

        return $mergedJobs;
    }

    /**
     * Evaluate job sentences words and packs them into an array
     *
     * @param string $sentencePart
     * @param int $key
     * @return array
     */
    private function evaluateSentencePartWords(string $sentencePart, int $key): array
    {
        $assetFlag = 0;
        $clauseParts = [];
        $sentencePart = ltrim($sentencePart);

        $sentencePartArray = explode(" ", $sentencePart);

        foreach ($sentencePartArray as $key2 => $word) {
            //get the first operator after comma sign as it is an operator between parts of a sentences
            if ($key > 0 && $key2 == 0) {
                $clauseParts[]['sentence_operator'] = $word;
                $assetFlag = 0;
                continue;
            }

            if ($word == static:: OR) {
                $clauseParts[]['operator'] = static:: OR;
                $assetFlag = 0;
            } else if ($word == static:: AND) {
                $clauseParts[]['operator'] = static:: AND;
                $assetFlag = 0;
            } else {
                if ($assetFlag) {
                    $prevIndex = count($clauseParts) - 1;
                    $clauseParts[$prevIndex]['asset'] .= ' ' . $word;
                } else {
                    $clauseParts[]['asset'] = $word;
                    $assetFlag = $clauseParts;

                }
            }
        }

        return $clauseParts;
    }

    /**
     * As app logic is iterating over my assets against job requirements, this function merges my assets results
     * for the same job, ie if the job suites against one asset, than it is fully suitable
     *
     * @param array $evaluatedJobs
     * @return void
     */
    private function mergeJobEvaluationsByMyAssets(array $evaluatedJobs): void
    {
        foreach ($evaluatedJobs as $key => $jobResultsByMyAssets) {
            foreach ($jobResultsByMyAssets as $myAsset => $value) {
                if ($value) {
                    $this->addFittingJob($myAsset, $key);
                }
            }
        }
    }

    /**
     * Iterates Jobs by assets
     *
     * @param array $formattedJobs
     * @param string $myAsset
     * @return array
     */
    private function iterateJobsByAsset(array $formattedJobs, string $myAsset): array
    {
        $evaluatedJobs = [];

        foreach ($formattedJobs as $key => $formattedJob) {
            $evaluatedJobs = array_merge($evaluatedJobs, $this->matchJobsAndAssets($key, $formattedJob, $myAsset));
        }

        return $evaluatedJobs;
    }

    /**
     * Logically matches job request words against my assets
     *
     * @param int $key
     * @param array $formattedJob
     * @param string $myAsset
     * @return mixed
     */
    private function matchJobsAndAssets(int $key, array $formattedJob, string $myAsset)
    {
        //this variable holds an info about the word type(asset of operator)
        $nextWordIndex = -1;
        //comma is dividing a sentences in a segments
        $sentenceSegment = 1;
        $evaluatedJobs[$key][$myAsset][$sentenceSegment] = false;

        foreach ($formattedJob as $key2 => $value) {
            //if previous word was an operator, this round is skipped as it is evaluated in previous
            if ($nextWordIndex == $key2) {
                continue;
            }

            foreach ($value as $type => $word) {
                switch ($type) {
                    case "asset":
                        $evaluatedJobs[$key][$myAsset][$sentenceSegment] = ($word == $myAsset);
                        break;
                    case "operator":
                        if ($word == static:: OR) {
                            $nextWordAsset = $formattedJob[$key2 + 1]["asset"];
                            $evaluatedJobs[$key][$myAsset][$sentenceSegment] =
                                $evaluatedJobs[$key][$myAsset][$sentenceSegment] || ($nextWordAsset == $myAsset);
                            $nextWordIndex = $key2 + 1;
                        } else {
                            //operator 'and'
                            $nextWordAsset = $formattedJob[$key2 + 1]["asset"];
                            $evaluatedJobs[$key][$myAsset][$sentenceSegment]
                                = $evaluatedJobs[$key][$myAsset][$sentenceSegment] && ($nextWordAsset == $myAsset);
                            $nextWordIndex = $key2 + 1;
                        }
                        break;
                    case "sentence_operator":
                        $sentenceSegment++;
                        if ($word == static:: OR) {
                            $nextWordAsset = $formattedJob[$key2 + 1]["asset"];
                            $previousValueForTheSameJobAssetSegment =
                                isset($evaluatedJobs[$key][$myAsset][$sentenceSegment])
                                    ? $evaluatedJobs[$key][$myAsset][$sentenceSegment]
                                    : true;
                            $evaluatedJobs[$key][$myAsset][$sentenceSegment] =
                                $previousValueForTheSameJobAssetSegment || ($nextWordAsset == $myAsset);
                            $nextWordIndex = $key2 + 1;
                        } else {
                            //operator 'and'
                            $nextWordAsset = $formattedJob[$key2 + 1]["asset"];
                            $previousValueForTheSameJobAssetSegment =
                                isset($evaluatedJobs[$key][$myAsset][$sentenceSegment])
                                    ? $evaluatedJobs[$key][$myAsset][$sentenceSegment]
                                    : true;
                            $evaluatedJobs[$key][$myAsset][$sentenceSegment] =
                                $previousValueForTheSameJobAssetSegment && ($nextWordAsset == $myAsset);
                            $nextWordIndex = $key2 + 1;
                        }
                        break;
                }
            }
        }

        return $evaluatedJobs;
    }
}
