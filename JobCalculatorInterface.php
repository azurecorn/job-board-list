<?php

/**
 * Interface JobCalculatorInterface
 */
interface JobCalculatorInterface
{
    /**
     * @param array $jobBoardListing
     * @return Model\Job[]
     */
    public function findSuitableJobs(array $jobBoardListing): array ;
}