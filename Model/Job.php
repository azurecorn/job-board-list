<?php

declare(strict_types=1);

namespace Model;

/**
 * Class Job
 *
 * This class doesn't hold much logic
 * Acts like a skeleton class for potential further development
 */
class Job
{
    private $myAsset = '';
    private $jobRequirement = '';

    /**
     * @param string|null $myAsset
     * @param int $jobRequirementKey
     */
    public function __construct($myAsset, int $jobRequirementKey)
    {
        $this->myAsset = $myAsset;
        $this->jobRequirement = \Data::jobsList()[$jobRequirementKey];
    }

    /**
     * String presentation of a Job object
     *
     * @return string
     */
    public function __toString(): string
    {
        if (is_null($this->myAsset)) {
            return "Bingo! This job doesn't require anything: {$this->jobRequirement}<br><br>";
        } else {
            return "Bingo! Job which suites your asset '{$this->myAsset}' is: {$this->jobRequirement}<br><br>";
        }
    }
}