<?php

declare(strict_types=1);

/**
 * Entry point of an app
 */

require "JobCalculator.php";
require "Data.php";

$jobCalculator = new JobCalculator(Data::myAssets());

$suitableJobs = $jobCalculator->findSuitableJobs(Data::jobsList());

//here we are echoing result jobs, as an result example

/** @var $suitableJobs Model\Job[] */
foreach ($suitableJobs as $suitableJob) {
    echo $suitableJob;
}